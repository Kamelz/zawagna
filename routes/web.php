<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

// Route::get('/test', function () {
// 	$attributes = \App\Attribute::with('group')->get();
	
// 	$groups =	$attributes->pluck('group')
// 		->groupBy('name')
// 		->map(function($group){
// 			return [
// 				'name' => $group->first()->name,
// 				'id' => $group->first()->id,
// 			];
// 		});

//     return view('test')->with([
//     	'attributes' => $attributes,
//     	'groups' => $groups
//     ]);
// });

Auth::routes(['verify' => true]);
Route::get('/auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('/auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');


Route::middleware(['auth'])->group(function () {

	Route::get('/home', 'HomeController@index')->name('home');

	Route::post('/attribute/{user}', 'AttributeController@update');
	Route::get('/attribute', 'AttributeController@index');
});