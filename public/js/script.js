$(document).ready(function(){
	var today = new Date();
	$('.datepicker').datepicker({
	    format: 'yyyy-mm-dd',
	    endDate: "today",
        maxDate: today,
	    todayHighlight: true  
	});


function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#uploaded_image').attr('src', e.target.result);
      $('#uploaded_image').css('display','block');
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#profile_picture").change(function() {
  readURL(this);
});

});