@extends('layouts.app')

<?php

$apperance = include __DIR__.'/../../../database/seeds/attributes/apperance.php';
$lifestyle = include __DIR__.'/../../../database/seeds/attributes/lifestyle.php';
$background = include __DIR__.'/../../../database/seeds/attributes/background.php';

$default = $apperance + $lifestyle + $background;
?>

@section('content')
<form method="POST" action="/attribute/{{auth()->user()->id}}">
   @csrf
<div class="container">
 @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
@endif
    <div class="row justify-content-center">

    	@foreach($groups as $group)

        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{__('groups.'.$group['name'])}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

					@foreach($attributes as $attribute)
               			@if($attribute->group_id === $group['id'])
						<div class="form-group row">
							<label for="gender" class="col-md-4 col-form-label text-md-right">
								{{ __('attributes.'.$attribute->name) }}
						
							</label>
							
							<div class="col-md-6">
								
								@if($default[$attribute->name] === 'text')
									@if($attribute->name === 'income')
									<input name="{{$attribute->name}}" style='width: 79px;' class="form-control" type="text">
									<select class="form-control" name="{{$attribute->name}}">
										@foreach($default['currencies'] as $currency)
											<option value="{{$currency}}">{{$currency}}</option>
										@endforeach
									</select>
									@else
									<input name="{{$attribute->name}}" class="form-control" type="text">
									@endif
								@endif	

								@if($default[$attribute->name] === 'textarea')
									<textarea class="form-control" name="{{$attribute->name}}"></textarea>
								@endif

								@if(isset($default[$attribute->name]['checkbox']))
									@foreach($default[$attribute->name]['data'] as $data)

									<input name="{{$attribute->name}}" class="form-check-input" type="checkbox" id="{{$data}}">

									<label class="form-check-label" for="{{$data}}">
										{{__('attributes_values.'.$data)}}
									</label> <br>
									@endforeach
								@endif
								@if( !isset($default[$attribute->name]['checkbox']) && is_array($default[$attribute->name]) )
									<!-- then it's a select tag -->
									<select class="form-control" name="{{$attribute->name}}">
										<option value="">{{__('attributes_values.no_select')}}</option>
										@foreach($default[$attribute->name] as $option)

											@if($attribute->name === 'height')
											
											<option value="{{$option}}">
											 {{\Lang::has('attributes_values.'.$option)?__('attributes_values.'.$option):$option}} {{__('attributes_values.cm')}}
											 </option>
								
											@elseif($attribute->name === 'weight')
											<option value="{{$option}}">
											 {{\Lang::has('attributes_values.'.$option)?__('attributes_values.'.$option):$option}} {{__('attributes_values.kg')}}
											 </option>

											@else
											<option value="{{$option}}">
											 {{\Lang::has('attributes_values.'.$option)?__('attributes_values.'.$option):$option}}
											 </option>
											@endif
										@endforeach
									</select>

								@endif
							</div>
						</div>
						@endif
                        @endforeach
				
                </div>

            </div>
        </div>
        <br>
        @endforeach

    </div>
    <div class="row justify-content-center">

    	<div class="col-md-10">
            <div class="card">


                <div class="card-body">
                	   		<div class="form-group row mb-0">
						<div class="col-md-6 offset-md-5">
							<button type="submit" class="btn btn-primary">
							{{ __('panel.submit') }}
							</button>
						</div>
					</div>
                </div>
            </div>
        </div>

    </div>
</div>
</form>
@endsection
