<?php

return [

    'email' => 'البريد الإلكترونى',
    'password' => 'كلمة المرور',
    'remember' => ' تذكرنى',
    'forgot' => 'نسيت كلمة المرور؟',
    'login' => 'تسجيل',
    'affiliate' => 'انضم',
    'logout' => 'تسجيل خروج',
];
