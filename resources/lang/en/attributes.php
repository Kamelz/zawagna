<?php

return [

    'hair_color' => "Hair color",
    
    'hair_length' => "Hair length",
    
    'hair_type' => "Hair type",
    
    'eye_color' => "Eye color",
    
    'eye_wear' => "Eye wear",
    
    'height' => "Height",
    
    'weight' => "Weight",
    
    'body' => "Body type",
    
    'ethnicity' => "Your ethnicity is mostly",
    
    'complexion' => "Complexion",
    
    'face_hair' => "Facial hair",
    
    'attractive' => "I consider my appearance as",
    
    'health' => "Physical / Health status",
    
    'drink' => "Do you drink?",
    
    'smoke' => "Do you smoke?",
    
    'eating' => "Eating Habits",
   
    'marital_status' => "Marital Status",
    
    'children' => "Do you have children?",
    
    'number_children' => "Number of children",
    
    'oldest_child' => "Oldest child",
    
    'youngest_child' => "Youngest child",
    
    'more_children' => "Do you want (more) children?",
    
    'occupation' => "Occupation",
    
    'employment' => "Employment status",
    
    'income' => "Annual Income",
    
    'home_type' => "Home type",
    
    'living_situation' => "Living situation",
    
    'residency' => "Residency status",
    
    'relocate' => "Willing to relocate",
    
    'relationship' => "Relationship you're looking for",
    
    'nationality' => "Nationality",
    
    'education' => "nationality",
    
    'languages' => "Languages spoken",
    
    'religion' => "Religion",
    
    'born_reverted' => "Born / Reverted",
    
    'religious_values' => "Religious values",
    
    'religiou_services' => "Attend religious services",

    'read' => "Read Qur'an",
    
    'polygamy' => "Polygamy",

    'family_values' => "Family values",

    'profile_creator' => "Profile creator",
    
    'profile_heading' => "Your profile heading",
    
    'info' => "A little about yourself",
    
    'looking_for' => "What you're looking for in a partner",


];