<?php

return [
    'apperance' => 'Your Appearance',
    'lifestyle' => 'Your Lifestyle',
    'background' => 'Your Background / Cultural Values',
    'moreinfo' => 'In your own words',
];
