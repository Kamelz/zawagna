<?php

return [
    'currencies'            =>       'Currencies',
    'no_select'             =>       'Please Select...',
    'bald_shaved'           =>       'Bald / Shaved',
    'black'                 =>       'Black',
    'blonde'                =>       'Blonde',
    'brown'                 =>       'Brown',
    'grey_white'            =>       'Grey / White',
    'light_brown'           =>       'Light Brown',
    'red'                   =>       'Red',
    'changes_frequently'    =>       'Changes frequently',
    'other'                 =>       'Other',
    'prefer_not_to_say'     =>       'Prefer not to say',


    'bald'                  =>      'Bald',
    'bald_on_top'           =>      'Bald on Top',
    'shaved'                =>      'Shaved',
    'short'                 =>      'Short',
    'medium'                =>      'Grey / White',
    'long'                  =>      'Light Brown',


    'straight'              =>      'Straight',
    'wavy'                  =>      'Wavy',
    'curly'                 =>      'Curly',



    'blue'                  =>      'Blue',
    'brown'                 =>      'Brown',
    'green'                 =>      'Green',
    'grey'                  =>      'Grey',
    'hazel'                 =>      'Hazel',


    'contacts'              =>      'Contacts',
    'glasses'               =>      'Glasses',
    'none'                  =>      'None',

    'cm'                    =>      'cm',

    'kg'                    =>      'kg',

    'petite'                => 'Petite',
    'slim'                  => 'Slim',
    'athletic'              => 'Athletic',
    'average'               => 'Average',
    'few_extra_pounds'      => 'Few Extra Pounds',
    'full_figured'          => 'Full Figured',
    'large_and_lovely'      => 'Large and Lovely',


    'arab'                  => 'Arab',
    'asian'                 => 'Asian',
    'black'                 => 'Black',
    'caucasian'             => 'Caucasian',
    'latino'                => 'Latino',
    'indian'                => 'Indian',
    'mixed'                 => 'Mixed',
    'pacific'               => 'Pacific',

    'very_fair'             => 'Very Fair',
    'fair'                  => 'Fair',
    'wheatish'              => 'Wheatish',
    'wheatish_brown'        => 'Wheatish Brown',
    'dark'                  => 'Dark',


    'clean_shaven'          => 'Clean Shaven',
    'sideburns'             => 'Sideburns',
    'mustache'              => 'Mustache',
    'goatee'                => 'Goatee',
    'sideburns'             => 'Sideburns',
    'goatee'                => 'Goatee',
    'medium_beard'          => 'Medium Beard',
    'long_beard'            => 'Long Beard',


    'below_average'         => 'Below average',
    'average'               => 'Average',
    'attractive'            => 'Attractive',
    'very_attractive'       => 'Very attractive',



    'normal'                => 'Normal',
    'minor_issues'          => 'Minor Health Issues',
    'serious_issues'        => 'Serious Health Issues',
    'minor_physical'        => 'Minor Physical Disability',
    'major_physical'        => 'Major Physical Disability',


    'donot_drink'           => "Don't drink",
    'occasionally_drink'    => "Occasionally drink",
    'do_drink'              => "Do drink",
    'Prefer not to say'     => "Prefer not to say",


    'do_smoke'              => 'Do smoke',
    'donot_smoke'           => "Don't smoke",
    'occasionally_smoke'    => 'Occasionally smoke',

    'halal_always'          => 'Halal foods always',
    'hala_when'             => 'Halal foods when I can',
    'no_restrictions'       => 'No special restrictions',


    'single'                => 'Single',
    'separated'             => 'Separated',
    'widowed'               => 'Widowed',
    'divorced'              => 'Divorced',
    'widowed'               => 'Widowed',

    'more_than_8'           => 'More Than 8',
    'older_than_18'         => 'Older than 18',


    'donot_live_at_home'    => "Yes - don't live at home",
    'sometimes_live_at_home'=> "Yes - sometimes live at home",
    'live_at_home'          => "Yes - live at home",
    

    'yes'                   => 'Yes',
    'no'                    => 'No',
    'not_sure'              => 'Not Sure',


    'administrative_secretarial_clerica'    => 'Administrative / Secretarial / Clerica',
    'advertising_media'                     => 'Advertising / Media',
    'artistic_creative_performance'         => 'Artistic / Creative / Performance',
    'construction_trades'                   => 'Construction / Trades',
    'domestic_gelper'                       => 'Domestic Helper',
    'education_academic'                    => 'Education / Academic',
    'executive_management_hr'               => 'Executive / Management / HR',
    'farming_agriculture'                   => 'Farming / Agriculture',
    'finance_banking_real_rstate'           => 'Finance / Banking / Real Estate',
    'fire_law_enforcement_security'         => 'Fire / law enforcement / security',
    'hair_dresser_personal_grooming'        => 'Hair Dresser / Personal Grooming',
    'it_communications'                     => 'IT / Communications',
    'laborer_manufacturing'                 => 'Laborer / Manufacturing',
    'legal'                                 => 'Legal',
    'medical_dental_veterinary'             => 'Medical / Dental / Veterinary',
    'military'                              => 'Military',
    'nanny_child_care'                      => 'Nanny / Child care',
    'no_occupation_stay_at_home'            => 'No occupation / Stay at home',
    'non_profit_clergy_social_services'     => 'Non-profit / clergy / social services',
    'political_govt_civil_service'          => 'Political / Govt / Civil Service',
    'retail_food_services'                  => 'Retail / Food services',
    'retired'                               => 'Retired',
    'sales_marketing'                       => 'Sales / Marketing',
    'self_Employed'                         => 'Self Employed',
    'sports_recreation'                     => 'Sports / recreation',
    'student'                               => 'Student',
    'technical_science_engineering'         => 'Technical / Science / Engineering',
    'transportation'                        => 'Transportation',
    'travel_Hospitality'                    => 'Travel / Hospitality',
    'unemployed'                            => 'Unemployed',



    'student'                               => 'Student',
    'part_time'                             => 'Part Time',
    'full_time'                             => 'Full Time',
    'homemaker'                             => 'Homemaker',
    'retired'                               => 'Retired',
    'not_employed'                          => 'Not Employed',


    'apartment_flat'                        => 'Apartment / Flat',
    'condominium'                           => 'Condominium',
    'farm'                                  => 'Farm',
    'house'                                 => 'House',
    'town_house'                            => 'Town house',


    'live_alone'                            => 'Live Alone',
    'live_with_friends'                     => 'Live with friends',
    'live_with_family'                      => 'Live with family',
    'live_with_kids'                        => 'Live with kids',
    'live_with_spouse'                      => 'Live with spouse',




    'citizen'                               => 'Citizen',
    'permanent_resident'                    => 'Permanent Resident',
    'work_permit'                           => 'Work Permit',
    'student_visa'                          => 'Student Visa',
    'temporary_visa'                        => 'Temporary Visa',



    'relocate_within_country'               => 'Willing to relocate within my country',
    'relocate_another_country'              => 'Willing to relocate to another country',
    'no_relocate'                           => 'Not willing to relocate',
    'not_sure_about_relocating'             => 'Not sure about relocating',


    'marriage'                              => 'Marriage',
    'friendship'                            => 'Friendship',


    'afghanistan' =>         "Afghanistan",
                            
                                
         'albania' =>         "Albania",
                            
                                
         'algeria' =>         "Algeria",
                            
                                
          'american Samoa' =>         "American Samoa",
                            
                                
         'andorra' =>         "Andorra",
                            
                                
          'angola' =>         "Angola",
                            
                                
          'anguilla' =>         "Anguilla",
                            
                                
          'antarctica' =>         "Antarctica",
                            
                                
         'antigua_and_barbuda' =>         "Antigua and Barbuda",
                            
                                
          'argentina' =>         "Argentina",
                            
                                
         'armenia' =>         "Armenia",
                            
                                
         'aruba' =>         "Aruba",
                            
                                
          'australia' =>         "Australia",
                            
                                
          'austria' =>         "Austria",
                            
                                
         'azerbaijan' =>         "Azerbaijan",
                            
                                
          'bahamas' =>         "Bahamas",
                            
                                
          'bahrain' =>         "Bahrain",
                            
                                
          'bangladesh' =>         "Bangladesh",
                            
                                
          'barbados' =>         "Barbados",
                            
                                
          'belarus' =>         "Belarus",
                            
                                
          'belgium' =>         "Belgium",
                            
                                
          'belize' =>         "Belize",
                            
                                
          'benin' =>         "Benin",
                            
                                
          'bermuda' =>         "Bermuda",
                            
                                
          'bhutan' =>         "Bhutan",
                            
                                
          'bolivia' =>         "Bolivia",
                            
                                
          'bosnia_herzegovina' =>         "Bosnia &amp; Herzegovina",
                            
                                
          'botswana' =>         "Botswana",
                            
                                
          'bouvet Island' =>         "Bouvet Island",
                            
                                
          'brazil' =>         "Brazil",
                            
                                
           'british_indian_ocean' =>         "British Indian Ocean",
                            
                                
          'brunei' =>         "Brunei",
                            
                                
          'bulgaria' =>         "Bulgaria",
                            
                                
           'burkina_faso' =>         "Burkina Faso",
                            
                                
          'burundi' =>         "Burundi",
                            
                                
          'cambodia' =>         "Cambodia",
                            
                                
          'cameroon' =>         "Cameroon",
                            
                                
          'canada' =>         "Canada",
                            
                                
          'cape_verde' =>         "Cape Verde",
                            
                                
          'cayman_islands' =>         "Cayman Islands",
                            
                                
          'central_african_rep' =>         "Central African Rep.",
                            
                                
          'chad' =>         "Chad",
                            
                                
          'chile' =>         "Chile",
                            
                                
          'china' =>         "China",
                            
                                
           'christmas_island' =>         "Christmas Island",
                            
                                
          'cocos' =>         "Cocos (Keeling) Is.",
                            
                                
          'colombia' =>         "Colombia",
                            
                                
          'comoros' =>         "Comoros",
                            
                                
          'congo' =>         "Congo, Dem. Rep",
                            
                                
          'Congo_republic' =>         "Congo, Republic",
                            
                                
          'cook_islands' =>         "Cook Islands",
                            
                                
          'costa_rica' =>         "Costa Rica",
                            
                                
           'cote_d_ivoire' =>         "Cote d'Ivoire",
                            
                                
          'croatia' =>         "Croatia",
                            
                                
          'cuba' =>         "Cuba",
                            
                                
          'cyprus' =>         "Cyprus",
                            
                                
          'czech_republic' =>         "Czech Republic",
                            
                                
          'denmark' =>         "Denmark",
                            
                                
          'djibouti' =>         "Djibouti",
                            
                                
          'dominica' =>         "Dominica",
                            
                                
          'dominican_republic' =>         "Dominican Republic",
                            
                                
           'dast_timor' =>         "East Timor",
                            
                                
          'ecuador' =>         "Ecuador",
                            
                                
          'egypt' =>         "Egypt",
                            
                                
          'el_salvador' =>         "El Salvador",
                            
                                
          'equatorial_guinea' =>         "Equatorial Guinea",
                            
                                
          'eritrea' =>         "Eritrea",
                            
                                
          'estonia' =>         "Estonia",
                            
                                
          'ethiopia' =>         "Ethiopia",
                            
                                
          'falkland_islands' =>         "Falkland Islands",
                            
                                
          'faroe_islands' =>         "Faroe Islands",
                            
                                
          'fiji' =>         "Fiji",
                            
                                
          'finland' =>         "Finland",
                            
                                
          'france' =>         "France",
                            
                                
          'french Antarctic' =>         "French Antarctic",
                            
                                
          'french Guiana' =>         "French Guiana",
                            
                                
          'french Polynesia' =>         "French Polynesia",
                            
                                
          'gabon' =>         "Gabon",
                            
                                
          'gambia' =>         "Gambia",
                            
                                
          'georgia' =>         "Georgia",
                            
                                
          'germany' =>         "Germany",
                            
                                
          'ghana' =>         "Ghana",
                            
                                
          'gibraltar' =>         "Gibraltar",
                            
                                
          'greece' =>         "Greece",
                            
                                
          'greenland' =>         "Greenland",
                            
                                
          'grenada' =>         "Grenada",
                            
                                
          'guadeloupe' =>         "Guadeloupe",
                            
                                
          'guam' =>         "Guam",
                            
                                
          'guatemala' =>         "Guatemala",
                            
                                
          'guernsey' =>         "Guernsey",
                            
                                
          'guinea' =>         "Guinea",
                            
                                
          'guinea_bissau' =>         "Guinea-Bissau",
                            
                                
          'guyana' =>         "Guyana",
                            
                                
          'haiti' =>         "Haiti",
                            
                                
          'heard_mcdonald' =>         "Heard &amp; McDonald Is",
                            
                                
           'holy_see' =>         "Holy See (Vatican City)",
                            
                                
          'honduras' =>         "Honduras",
                            
                                
          'hong_hong' =>         "Hong Kong (China)",
                            
                                
          'hungary' =>         "Hungary",
                            
                                
          'iceland' =>         "Iceland",
                            
                                
          'india' =>         "India",
                            
                                
           'indonesia' =>         "Indonesia",
                            
                                
           'iran' =>         "Iran",
                            
                                
           'iraq' =>         "Iraq",
                            
                                
          'ireland' =>         "Ireland",
                            
                                
           'isle_of_man' =>         "Isle of Man",
                            
                            
                            
                                
           'italy' =>         "Italy",
                            
                                
           'jamaica' =>         "Jamaica",
                            
                                
           'japan' =>         "Japan",
                            
                                
           'jersey' =>         "Jersey",
                            
                                
           'jordan' =>         "Jordan",
                            
                                
           'kazakhstan' =>         "Kazakhstan",
                            
                                
           'kenya' =>         "Kenya",
                            
                                
           'kiribati' =>         "Kiribati",
                            
                                
           'korea_north' =>         "Korea, North",
                            
                                
           'korea_south' =>         "Korea, South",
                            
                                
           'kuwait' =>         "Kuwait",
                            
                                
           'kyrgyzstan' =>         "Kyrgyzstan",
                            
                                
           'laos' =>         "Laos",
                            
                                
           'latvia' =>         "Latvia",
                            
                                
           'lebanon' =>         "Lebanon",
                            
                                
           'lesotho' =>         "Lesotho",
                            
                                
           'liberia' =>         "Liberia",
                            
                                
           'libya' =>         "Libya",
                            
                                
           'liechtenstein' =>         "Liechtenstein",
                            
                                
           'lithuania' =>         "Lithuania",
                            
                                
           'luxembourg' =>         "Luxembourg",
                            
                                
           'macau' =>         "Macau (China)",
                            
                                
           'macedonia' =>         "Macedonia (FYR)",
                            
                                
           'madagascar' =>         "Madagascar",
                            
                                
           'malawi' =>         "Malawi",
                            
                                
           'malaysia' =>         "Malaysia",
                            
                                
           'maldives' =>         "Maldives",
                            
                                
           'mali' =>         "Mali",
                            
                                
           'malta' =>         "Malta",
                            
                                
           'marshall_islands' =>         "Marshall Islands",
                            
                                
           'martinique' =>         "Martinique",
                            
                                
           'mauritania' =>         "Mauritania",
                            
                                
           'mauritius' =>         "Mauritius",
                            
                                
           'mayotte' =>         "Mayotte",
                            
                                
           'mexico' =>         "Mexico",
                            
                                
          'micronesia' =>         "Micronesia",
                            
                                
           'moldova' =>         "Moldova",
                            
                                
           'monaco' =>         "Monaco",
                            
                                
           'mongolia' =>         "Mongolia",
                            
                                
           'montserrat' =>         "Montserrat",
                            
                                
           'morocco' =>         "Morocco",
                            
                                
           'mozambique' =>         "Mozambique",
                            
                                
           'myanmar' =>         "Myanmar",
                            
                                
           'namibia' =>         "Namibia",
                            
                                
           'nauru' =>         "Nauru",
                            
                                
           'nepal' =>         "Nepal",
                            
                                
           'netherlands' =>         "Netherlands",
                            
                                
           'netherlands_antilles' =>         "Netherlands Antilles",
                            
                                
           'new_caledonia' =>         "New Caledonia",
                            
                                
           'new_zealand' =>         "New Zealand",
                            
                                
           'nicaragua' =>         "Nicaragua",
                            
                                
           'niger' =>         "Niger",
                            
                                
           'nigeria' =>         "Nigeria",
                            
                                
           'niue' =>         "Niue",
                            
                                
           'norfolk_island' =>         "Norfolk Island",
                            
                                
          'northern_mariana_is' =>         "Northern Mariana Is.",
                            
                                
           'norway' =>         "Norway",
                            
                                
           'oman' =>         "Oman",
                            
                                
           'pakistan' =>         "Pakistan",
                            
                                
           'palau' =>         "Palau",
                            
                                
          'palestine' =>         "Palestine",
                            
                                
           'panama' =>         "Panama",
                            
                                
           'papua_new_guinea' =>         "Papua New Guinea",
                            
                                
           'paraguay' =>         "Paraguay",
                            
                                
           'peru' =>         "Peru",
                            
                                
           'philippines' =>         "Philippines",
                            
                                
           'pitcairn_islands' =>         "Pitcairn Islands",
                            
                                
           'poland' =>         "Poland",
                            
                                
           'portugal' =>         "Portugal",
                            
                                
           'puerto_rico' =>         "Puerto Rico",
                            
                                
           'qatar' =>         "Qatar",
                            
                                
           'reunion' =>         "Reunion",
                            
                                
           'romania' =>         "Romania",
                            
                                
           'russia' =>         "Russia",
                            
                                
           'rwanda' =>         "Rwanda",
                            
                                
           'saint_helena' =>         "Saint Helena",
                            
                                
           'saint_lucia' =>         "Saint Lucia",
                            
                                
           'samoa' =>         "Samoa",
                            
                                
           'san_marino' =>         "San Marino",
                            
                                
           'sao_tome_principe' =>         "Sao Tome &amp; Principe",
                            
                                
           'saudi_arabia' =>         "Saudi Arabia",
                            
                                
           'senegal' =>         "Senegal",
                            
                                
           'serbia' =>         "Serbia",
                            
                                
           'seychelles' =>         "Seychelles",
                            
                                
           'sierra Leone' =>         "Sierra Leone",
                            
                                
           'singapore' =>         "Singapore",
                            
                                
           'slovakia' =>         "Slovakia",
                            
                                
           'slovenia' =>         "Slovenia",
                            
                                
          'solomon_islands' =>         "Solomon Islands",
                            
                                
           'somalia' =>         "Somalia",
                            
                                
           'south_africa' =>         "South Africa",
                            
                                
           'south_georgia_island' =>         "South Georgia Island",
                            
                                
           'spain' =>         "Spain",
                            
                                
          'sri_lanka' =>         "Sri Lanka",
                            
                                
           'st_kitts_nevis' =>         "St Kitts &amp; Nevis",
                            
                                
           'st_pierre_miquelon' =>         "St Pierre &amp;  Miquelon",
                            
                                
           'st_vincent_grenadines' =>         "St Vincent Grenadines",
                            
                                
           'sudan' =>         "Sudan",
                            
                                
           'suriname' =>         "Suriname",
                            
                                
           'svalbard_jan_mayen' =>         "Svalbard &amp; Jan Mayen",
                            
                                
           'swaziland' =>         "Swaziland",
                            
                                
           'sweden' =>         "Sweden",
                            
                                
           'switzerland' =>         "Switzerland",
                            
                                
           'syria' =>         "Syria",
                            
                                
           'taiwan' =>         "Taiwan (China)",
                            
                                
           'tajikistan' =>         "Tajikistan",
                            
                                
           'tanzania' =>         "Tanzania",
                            
                                
           'thailand' =>         "Thailand",
                            
                                
           'togo' =>         "Togo",
                            
                                
           'tokelau' =>         "Tokelau",
                            
                                
           'tonga' =>         "Tonga",
                            
                                
           'trinidad and Tobago' =>         "Trinidad and Tobago",
                            
                                
           'tunisia' =>         "Tunisia",
                            
                                
           'turkey' =>         "Turkey",
                            
                                
           'turkmenistan' =>         "Turkmenistan",
                            
                                
           'turks_caicos_is' =>         "Turks &amp; Caicos Is.",
                            
                                
           'tuvalu' =>         "Tuvalu",
                            
                                
           'uganda' =>         "Uganda",
                            
                                
           'ukraine' =>         "Ukraine",
                            
                                
         'united_arab_emirates' =>         "United Arab Emirates",
                            
                                
           'united_kingdom' =>         "United Kingdom",
                            
                                
           'united_states' =>         "United States",
                            
                                
           'uruguay' =>         "Uruguay",
                            
                                
           'us_minor_outlying_is' =>         "US Minor Outlying Is",
                            
                                
           'uzbekistan' =>         "Uzbekistan",
                            
                                
           'vanuatu' =>         "Vanuatu",
                            
                                
           'venezuela' =>         "Venezuela",
                            
                                
           'vietnam' =>         "Vietnam",
                            
                                
           'virgin_islands_british' =>         "Virgin Islands (British)",
                            
                                
           'virgin_islands_us' =>         "Virgin Islands (US)",
                            
                                
           'wallis_and_futuna' =>         "Wallis and Futuna",
                            
                                
           'western_sahara' =>         "Western Sahara",
                            
                                
           'yemen' =>         "Yemen",
                            
                                
           'zambia' =>         "Zambia",
                            
                                
           'zimbabwe' =>         "Zimbabwe",
                            

            'primary'                       => 'Primary (Elementary) School',
            'middle_school'                  => 'Middle School / Junior High',
            'high_school'                   => 'High School',
            'vocational'                    => 'Vocational College',
            'bachelors'                     => 'Bachelors Degree',
            'masters'                       => 'Masters Degree',
            'phd'                           => 'PhD or Doctorate',

            'arabic'=>"Arabic",

            'english'=>"English",

            'french'=>"French",

            'pidgin'=>"Pidgin",

            'urdu'=>"Urdu",

            'german'=>"German",

            'spanish'=>"Spanish",

            'afrikaans'=>"Afrikaans",

            'albanian'=>"Albanian",

            'amharic'=>"Amharic",

            'armenian'=>"Armenian",

            'assyrian'=>"Assyrian",

            'azerbaijani'=>"Azerbaijani",


            'bahasa_malay_indonesian'=>"Bahasa Malay / Indonesian",


            'belorussian'=>"Belorussian",

            'bengali'=>"Bengali",

            'berber'=>"Berber",

            'bulgarian'=>"Bulgarian",

            'burmese'=>"Burmese",

            'cebuano'=>"Cebuano",

            'chinese'=>"Chinese (Cantonese)",

            'chinese'=>"Chinese (Mandarin)",

            'creole'=>"Creole",

            'croatian'=>"Croatian",

            'czech'=>"Czech",

            'danish'=>"Danish",

            'dutch'=>"Dutch",

            'eritrean'=>"Eritrean",

            'estonian'=>"Estonian",

            'farsi'=>"Farsi",

            'finnish'=>"Finnish",

            'georgian'=>"Georgian",

            'greek'=>"Greek",

            'gujarati'=>"Gujarati",

            'hausa'=>"Hausa",

            'hebrew'=>"Hebrew",

            'hindi'=>"Hindi",

            'hungarian'=>"Hungarian",

            'icelandic'=>"Icelandic",

            'iilocano'=>"Iilocano",

            'indonesian'=>"Indonesian",

            'inuktitut'=>"Inuktitut",

            'italian'=>"Italian",

            'japanese'=>"Japanese",

            'kannada'=>"Kannada",

            'kazakh'=>"Kazakh",

            'khmer'=>"Khmer",

            'kirgiz'=>"Kirgiz",

            'korean'=>"Korean",

            'kurdish'=>"Kurdish",

            'kutchi'=>"Kutchi",

            'kyrgiz'=>"Kyrgiz",

            'laotian'=>"Laotian",

            'latvian'=>"Latvian",

            'lithuanian'=>"Lithuanian",

            'macedonian'=>"Macedonian",

            'malagasy'=>"Malagasy",

            'malayalam'=>"Malayalam",

            'maldivian'=>"Maldivian",

            'maltese'=>"Maltese",

            'marathi'=>"Marathi",

            'mongolian'=>"Mongolian",

            'nepali'=>"Nepali",

            'norwegian'=>"Norwegian",

            'pashto'=>"Pashto",

            'persian'=>"Persian",

            'polish'=>"Polish",

            'portuguese'=>"Portuguese",

            'quechua'=>"Quechua",

            'romanian'=>"Romanian",

            'russian'=>"Russian",

            'serbian'=>"Serbian",

            'sindhi'=>"Sindhi",

            'sinhala'=>"Sinhala",

            'slovak'=>"Slovak",

            'slovene'=>"Slovene",

            'somali'=>"Somali",

            'swahili'=>"Swahili",

            'swedish'=>"Swedish",

            'tagalog'=>"Tagalog",

            'tamil'=>"Tamil",

            'telugu'=>"Telugu",

            'thai'=>"Thai",

            'tibetan'=>"Tibetan",

            'tongan'=>"Tongan",

            'turkish'=>"Turkish",

            'turkmen'=>"Turkmen",

            'ugaritic'=>"Ugaritic",

            'ukrainian'=>"Ukrainian",

            'uzbek'=>"Uzbek",

            'vietnamese'=>"Vietnamese",

            'welsh'=>"Welsh",

            'islam_sunni' => 'Islam - Sunni',
            'islam_shiite' => 'Islam - Shiite',
            'islam_sufism' => 'Islam - Sufism',
            'islam_ahmadiyya' => 'Islam - Ahmadiyya',
            'islam_other' => 'Islam - Other',
            'wlling_to_revert' => 'Willing to revert',


            'born_a_muslim' => 'Born a muslim',
            'reverted_to_islam' => 'Reverted to Islam',
            'plan_to_revert_to_islam' => 'Plan to revert to Islam',


            'very_religious' => 'Very Religious',
            'religious' => 'Religious',
            'not_religious' => 'Not Religious',

            'daily' => 'Daily',
            'only_on_Jummah_fridays' => 'Only on Jummah / Fridays',
            'only_during_ramadan' => 'Only During Ramadan',
            'sometimes' => 'Sometimes',
            'never' => 'Never',

            'ocassionally' => 'Ocassionally',
            'read_translated_version' => 'Read translated version',
            'never_Read' => 'Never Read',

            'accept_polygamy' => 'Accept polygamy',
            'maybe_accept_polygamy' => 'Maybe accept polygamy',
            "donot_accept_polygamy" => "Don't accept polygamy",


            'conservative' => 'Conservative',
            'moderate' => 'Moderate',
            'liberal' => 'Liberal',


            'self' => 'Self',
            'parent' => 'Parent',
            'friend' => 'Friend',
            'brother_sister' => 'Brother / Sister',
            'relative' => 'Relative',


];
                                