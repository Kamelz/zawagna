<?php

use App\Group;
use App\Attribute;
use Illuminate\Database\Seeder;

class AttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Group::count() === 0){
        	$apperance = Group::create(['name'=>'apperance']);
        	$lifestyle = Group::create(['name'=>'lifestyle']);
        	$background = Group::create(['name'=>'background']);
        	$info = Group::create(['name'=>'moreinfo']);
        }
        
        // $defaultApperance = include __DIR__.'../attributes/apperance.php';
        // $defaultLifestyle = include __DIR__.'../attributes/lifestyle.php';
        // $defaultBackground = include __DIR__.'../attributes/background.php';

        $attributes = [
            
            [
                'name' => 'hair_color',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['hair_color'])
            ],
            
            [
                'name' => 'hair_length',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['hair_length'])
            ],
            
            [
                'name' => 'hair_type',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['hair_type'])
            ],
            
            [
                'name' => 'eye_color',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['eye_color'])
            ],
                        [
                'name' => 'eye_wear',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['eye_wear'])
            ],
            
            [
                'name' => 'height',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['height'])
            ],
            
            [
                'name' => 'weight',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['weight'])
            ],
            
            [
                'name' => 'body',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['body'])
            ],
            
            [
                'name' => 'ethnicity',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['ethnicity'])
            ],
            
            [
                'name' => 'complexion',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['complexion'])
            ],
            
            [
                'name' => 'face_hair',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['face_hair'])
            ],
            
            [
                'name' => 'attractive',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['attractive'])
            ],
            
            [
                'name' => 'health',
                'group' => $apperance->id,
                // 'default' => serialize($defaultApperance['health'])
            ],
            
            [
                'name' => 'drink',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['drink'])
            ],
            
            [
                'name' => 'smoke',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['smoke'])
            ],
            
            [
                'name' => 'eating',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['eating'])
            ],
            
            [
                'name' => 'marital_status',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['marital_status'])
            ],
            
            [
                'name' => 'children',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['children'])
            ],
            
            [
                'name' => 'number_children',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['number_children'])
            ],
            
            [
                'name' => 'oldest_child',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['oldest_child'])
            ],
            
            [
                'name' => 'youngest_child',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['youngest_child'])
            ],
            
            [
                'name' => 'more_children',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['more_children'])
            ],
            
            [
                'name' => 'occupation',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['occupation'])
            ],
            
            [
                'name' => 'employment',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['employment'])
            ],
            
            [
                'name' => 'income',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['income'])
            ],
            
            [
                'name' => 'home_type',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['home_type'])
            ],
            
            [
                'name' => 'living_situation',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['living_situation'])
            ],
            
            [
                'name' => 'residency',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['residency'])
            ],
            
            [
                'name' => 'relocate',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['relocate'])
            ],
            
            [
                'name' => 'relationship',
                'group' => $lifestyle->id,
                // 'default' => serialize($defaultLifestyle['relationship'])
            ],
            
            [
                'name' => 'nationality',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['nationality'])
            ],
            
            [
                'name' => 'education',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['education'])
            ],
            
            [
                'name' => 'languages',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['languages'])
            ],
            
            [
                'name' => 'religion',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['religion'])
            ],
            
            [
                'name' => 'born_reverted',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['born_reverted'])
            ],
            
            [
                'name' => 'religious_values',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['religious_values'])
            ],
            
            [
                'name' => 'religiou_services',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['religiou_services'])
            ],
            
            [
                'name' => 'read',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['read'])
            ],
            
            [
                'name' => 'polygamy',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['polygamy'])
            ],

            [
                'name' => 'family_values',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['family_values'])
            ],

            
            [
                'name' => 'profile_creator',
                'group' => $background->id,
                // 'default' => serialize($defaultBackground['profile_creator'])
            ],

            
            [
                'name' => 'profile_heading',
                'group' => $info->id,
                // 'default' => serialize($defaultBackground['profile_heading'])
            ],

            
            [
                'name' => 'info',
                'group' => $info->id,
                // 'default' => serialize($defaultBackground['info'])
            ],

            
            [
                'name' => 'looking_for',
                'group' => $info->id,
                // 'default' => serialize($defaultBackground['looking_for'])
            ],
        ];

        if(Attribute::count() === 0){
            foreach ($attributes as $attribute) {
                Attribute::create([
                    'name' => $attribute['name'],
                    'group_id'=> $attribute['group'],
                    // 'default'=> $attribute['default'],
                ]);
            }
        }  
    }
}