<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->import(App\Country::class,'database/sql/countries.sql');
        $this->import(App\State::class,'database/sql/states.sql');
  		// $this->import(App\City::class,'database/sql/cities.sql'); toto import it manually
    }

    /**
     * Import sql files to database
     * @param  $model
     * @param  $sql    
     * @return void
     */
    public function import($model,$sql){

  	$modelNotEmpty = $model::count();
		if($modelNotEmpty === 0){
			DB::unprepared(file_get_contents(base_path($sql)));
		}
    }
}
