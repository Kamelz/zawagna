<?php

return [
	 
'nationality' => [
'afghanistan',
'albania',
'algeria',
'american Samoa',
'andorra',
'angola',
'anguilla',
'antarctica',
'antigua_and_barbuda',
'argentina',
'armenia',
'aruba',
'australia',
'austria',
'azerbaijan',
'bahamas',
'bahrain',
'bangladesh',
'barbados',
'belarus',
'belgium',
'belize',
'benin',
'bermuda',
'bhutan',
'bolivia',
'bosnia_herzegovina',
'botswana',
'bouvet Island',
'brazil',
'british_indian_ocean',
'brunei',
'bulgaria',
'burkina_faso',
'burundi',
'cambodia',
'cameroon',
'canada',
'cape_verde',
'cayman_islands',
'central_african_rep',
'chad',
'chile',
'china',
'christmas_island',
'cocos',
'colombia',
'comoros',
'congo',
'Congo_republic',
'cook_islands',
'costa_rica',
'cote_d_ivoire',
'croatia',
'cuba',
'cyprus',
'czech_republic',
'denmark',
'djibouti',
'dominica',
'dominican_republic',
'dast_timor',
'ecuador',
'egypt',
'el_salvador',
'equatorial_guinea',
'eritrea',
'estonia',
'ethiopia',
'falkland_islands',
'faroe_islands',
'fiji',
'finland',
'france',
'french Antarctic',
'french Guiana',
'french Polynesia',
'gabon',
'gambia',
'georgia',
'germany',
'ghana',
'gibraltar',
'greece',
'greenland',
'grenada',
'guadeloupe',

'guam',

'guatemala',

'guernsey',

'guinea',
'guinea_bissau',

'guyana',

'haiti',
'heard_mcdonald',

'holy_see',

'honduras',

'hong_hong',

'hungary',

'iceland',

'india',
'indonesia',

'iran',

'iraq',

'ireland',
'isle_of_man',

'italy',

'jamaica',

'japan',

'jersey',

'jordan',
'kazakhstan',

'kenya',

'kiribati',
'korea_north',
'korea_south',

'kuwait',
'kyrgyzstan',

'laos',

'latvia',

'lebanon',

'lesotho',

'liberia',

'libya',
'liechtenstein',
'lithuania',
'luxembourg',

'macau',
'macedonia',
'madagascar',

'malawi',

'malaysia',

'maldives',

'mali',

'malta',
'marshall_islands',
'martinique',
'mauritania',
'mauritius',

'mayotte',

'mexico',
'micronesia',

'moldova',

'monaco',

'mongolia',
'montserrat',

'morocco',
'mozambique',

'myanmar',

'namibia',

'nauru',

'nepal',
'netherlands',
'netherlands_antilles',
'new_caledonia',
'new_zealand',
'nicaragua',

'niger',

'nigeria',

'niue',
'norfolk_island',
'northern_mariana_is',

'norway',

'oman',

'pakistan',

'palau',

'palestine',

'panama',
'papua_new_guinea',

'paraguay',

'peru',
'philippines',
'pitcairn_islands',

'poland',

'portugal',
'puerto_rico',

'qatar',

'reunion',

'romania',

'russia',

'rwanda',
'saint_helena',
'saint_lucia',

'samoa',
'san_marino',
'sao_tome_principe',
'saudi_arabia',

'senegal',

'serbia',
'seychelles',
'sierra Leone',
'singapore',

'slovakia',

'slovenia',
'solomon_islands',

'somalia',
'south_africa',
'south_georgia_island',

'spain',

'sri_lanka',
'st_kitts_nevis',
'st_pierre_miquelon',
'st_vincent_grenadines',

'sudan',

'suriname',
'svalbard_jan_mayen',
'swaziland',

'sweden',
'switzerland',

'syria',

'taiwan',
'tajikistan',
'tanzania',

'thailand',

'togo',

'tokelau',

'tonga',
'trinidad and Tobago',

'tunisia',

'turkey',
'turkmenistan',
'turks_caicos_is',

'tuvalu',

'uganda',

'ukraine',
'united_arab_emirates',
'united_kingdom',
'united_states',

'uruguay',
'us_minor_outlying_is',
'uzbekistan',

'vanuatu',
'venezuela',

'vietnam',
'irgin_islands_british',
'virgin_islands_us',
'wallis_and_futuna',
'western_sahara',

'yemen',

'zambia',

'zimbabwe'

	],

	'education' => [

		'primary',
		'middle_school',  
		'high_school', 
		'vocational', 
		'bachelors',
		'masters', 
		'phd'
  ],
	
	'languages' => [
			'arabic',
			'english',
			'french',
			'pidgin',
			'urdu',
			'german',
			'spanish',
			'afrikaans',
			'albanian',
			'amharic',
			'armenian',
			'assyrian',
			'azerbaijani',
			'bahasa_malay_indonesian',
			'belorussian',
			'bengali',
			'berber',
			'bulgarian',
			'burmese',
			'cebuano',
			'chinese',
			'chinese',
			'creole',
			'croatian',
			'czech',
			'danish',
			'dutch',
			'eritrean',
			'estonian',
			'farsi',
			'finnish',
			'georgian',
			'greek',
			'gujarati',
			'hausa',
			'hebrew',
			'hindi',
			'hungarian',
			'icelandic',
			'iilocano',
			'indonesian',
			'inuktitut',
			'italian',
			'japanese',
			'kannada',
			'kazakh',
			'khmer',
			'kirgiz',
			'korean',
			'kurdish',
			'kutchi',
			'kyrgiz',
			'laotian',
			'latvian',
			'lithuanian',
			'macedonian',
			'malagasy',
			'malayalam',
			'maldivian',
			'maltese',
			'marathi',
			'mongolian',
			'nepali',
			'norwegian',
			'pashto',
			'persian',
			'polish',
			'portuguese',
			'quechua',
			'romanian',
			'russian',
			'serbian',
			'sindhi',
			'sinhala',
			'slovak',
			'slovene',
			'somali',
			'swahili',
			'swedish',
			'tagalog',
			'tamil',
			'telugu',
			'thai',
			'tibetan',
			'tongan',
			'turkish',
			'turkmen',
			'ugaritic',
			'ukrainian',
			'uzbek',
			'vietnamese',
			'welsh',
			'other',
	],
	
	'religion' => [

		'islam_sunni',
		'islam_shiite',
		'islam_sufism',
		'islam_ahmadiyya',
		'islam_other',
		'willing_to_revert',
		'other',
	],
	
	'born_reverted' => [
		'born_a_muslim',
		'reverted_to_islam',
		'plan_to_revert_to_islam',
	],
	
	'religious_values' => [
		'very_religious',
		'religious',
		'not_religious',
	],
	
	'religiou_services' => [
		'daily',
		'only_on_Jummah_fridays',
		'only_during_ramadan',
		'sometimes',
		'never'
	],

	'read' => [

		'daily' ,
		'only_during_ramadan' ,
		'only_on_Jummah_fridays' ,
		'ocassionally' ,
		'read_translated_version',
		'never_Read' ,
		'prefer_not_to_say' ,
	],

	'polygamy' => [
		'accept_polygamy',
		'maybe_accept_polygamy',
		"donot_accept_polygamy",
	],

	'family_values' => [

		'conservative',
		'moderate',
		'liberal',
		'prefer_not_to_say'      
	],

	'profile_creator' => [

		'self' ,
		'parent' ,
		'friend' ,
		'brother_sister',
		'relative' ,

	],

	'profile_heading' => 'text', 

	'info' => 'textarea',

	'looking_for' => 'textarea',
];



