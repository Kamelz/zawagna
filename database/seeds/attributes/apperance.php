<?php

$height = [];
$weight = [];

for($i=140; $i<220; $i++){$height[] +=$i;}

for($i=40; $i<220; $i++){$weight[] +=$i;}



	
return [
	'hair_color' => [

			'bald_shaved',
			'black',
			'blonde',
			'brown',
			'grey_white',
			'light_brown',
			'red',
			'changes_frequently',
			'other',
			'prefer_not_to_say',
	],

	'hair_length' => [

			'bald',
			'bald on Top',
			'blonde',
			'shaved',
			'short',
			'medium',
			'long',
			'changes_frequently',
			'other',
			'prefer_not_to_say',
	],

	'hair_type' => [

			'straight',
			'wavy',
			'curly',
			'other',
			'prefer_not_to_say',
	],

	'eye_color' => [

			'blue',
			'brown',
			'green',
			'grey',
			'hazel',
			'other',
	],

	'eye_wear' => [

			'contacts',
			'glasses',
			'none',
			'other',
			'prefer_not_to_say',
	],

	'height' => $height,


	'weight' => $weight,


	'body' => [
			'petite',
			'slim',
			'athletic',
			'average',
			'few_extra_pounds',
			'full_figured',
			'large_and_lovely' 
	],



	'ethnicity' => [
			'arab' ,
			'asian',
			'black',
			'caucasian',
			'latino',
			'indian' ,
			'mixed',   
			'pacific', 
			'other' 
	],


	'complexion' => [

			'very_fair',
			'fair',
			'wheatish',
			'wheatish_brown',
			'dark',
			'prefer_not_to_say',
	],


	'face_hair' => [
			'clean_shaven',
			'sideburns', 
			'mustache',
			'goatee',
			'sideburns', 
			'goatee',
			'medium_beard', 
			'long_beard',
			'other',
	],

	'attractive' => [
			'below_average',
			'average', 
			'attractive',
			'very_attractive' 
	],


	'health' => [
			'normal',
			'minor_issues',
			'serious_issues',
			'minor_physical', 
			'major_physical',
			'prefer_not_to_say'
	],

];

