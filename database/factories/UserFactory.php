<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
	$country = factory(App\Country::class)->create()->id;
	$state = factory(App\State::class)->create(['country_id' => $country])->id;
	$city = factory(App\City::class)->create(['state_id' => $state])->id;
    return [
        'name' => $faker->name,
        'age' => 18,
        'gender' => $faker->randomElement(['female', 'male']),
        'country_id' => $country,
        'state_id' => $state ,
        'city_id' => $city,
        'profile_strength' => 0,
        'is_admin' => 0,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'profile_picture' => '',
    ];
});