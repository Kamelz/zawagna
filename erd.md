# users
	- name
	- gender
	- country_id
	- state_id
	- is_admin
	- city
	- profile_strength

# photos
	- user_id
	- photo

# attribute_user
	- user_id
	- attribute_id
	- value

# attributes
	- key
	- value
	- group_id

# groups
	- name


# countries
	-sortname
	- name


# states
	- name
	- country_id


# cities
	- state_id
	- name