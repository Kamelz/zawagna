<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeUser extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attribute_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'user_id',
        'attribute',
    ];
}
