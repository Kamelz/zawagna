<?php

namespace App;

use App\Group;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'group_id',
        'default',
    ];

    /**
     * 
     * @return belongsTo
     */
    public function group(){

    	return $this->belongsTo(Group::class,'group_id');
    }
}
