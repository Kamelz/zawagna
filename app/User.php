<?php

namespace App;
use App\Attribute;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'age',
        'name',
        'email',
        'gender',
        'is_admin',
        'city_id',
        'birthday',
        'password',
        'state_id',
        'country_id',
        'facebook_id',
        'profile_picture',
        'profile_strength',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * 
     * @return HasRelationships
     */
    public function attributes(){

        return $this->belongsToMany(Attribute::class);
    }
}
