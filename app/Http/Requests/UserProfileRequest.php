<?php

namespace App\Http\Requests;
use App\AttributeUser;
use Illuminate\Foundation\Http\FormRequest;

class UserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->id === request('user')->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hair_color' => "nullable",

            'hair_length' => "nullable",

            'hair_type' => "nullable",

            'eye_color' => "nullable",

            'eye_wear' => "nullable",

            'height' => "nullable",

            'weight' => "nullable",

            'body' => "nullable",

            'ethnicity' => "nullable",

            'complexion' => "nullable",

            'face_hair' => "nullable",

            'attractive' => "nullable",

            'health' => "nullable",

            'drink' => "nullable",

            'smoke' => "nullable",

            'eating' => "nullable",

            'marital_status' => "nullable",

            'children' => "nullable",

            'number_children' => "nullable",

            'oldest_child' => "nullable",

            'youngest_child' => "nullable",

            'more_children' => "nullable",

            'occupation' => "nullable",

            'employment' => "nullable",

            'income' => "nullable",

            'home_type' => "nullable",

            'living_situation' => "nullable",

            'residency' => "nullable",

            'relocate' => "nullable",

            'relationship' => "nullable",

            'nationality' => "nullable",

            'education' => "nullable",

            'languages' => "nullable",

            'religion' => "nullable",

            'born_reverted' => "nullable",

            'religious_values' => "nullable",

            'religiou_services' => "nullable",

            'read' => "nullable",

            'polygamy' => "nullable",

            'family_values' => "nullable",

            'profile_creator' => "nullable",

            'profile_heading' => "nullable",

            'info' => "nullable",

            'looking_for' => "nullable"
        ];
    }

    public function persist(){
        $data =  $this->validated();
        foreach($data as $key => $value){
           $attribute =  [
                'user_id' => auth()->user()->id,
                'value' => $value,
                'attribute' => $key
            ];
            $method= AttributeUser::where('user_id',auth()->user()->id)
            ->where('attribute',$key)->count() === 0?'create':'update';

            AttributeUser::where('user_id',auth()->user()->id)
            ->where('attribute',$key)
            ->$method($attribute);
        }
        return $this;
    }
}
