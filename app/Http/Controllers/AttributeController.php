<?php

namespace App\Http\Controllers;
use App\User;
use App\Attribute;
use Illuminate\Http\Request;
use App\Http\Requests\UserProfileRequest;

class AttributeController extends Controller
{

	public function index(){

	$attributes = Attribute::with('group')->get();
	
	$groups =	$attributes->pluck('group')
		->groupBy('name')
		->map(function($group){
			return [
				'name' => $group->first()->name,
				'id' => $group->first()->id,
			];
		});

		$data = [
			'attributes' => $attributes,
			'groups' => $groups
		];
    return view('profile_form')->with($data);

	}
	/**
	 * Update user profile
	 * @param  UserProfileRequest $request 
	 * @return array                      
	 */
	public function update(UserProfileRequest $request, User $user){

		$request->persist();
		return redirect('/attribute')->with(['success' => 'Profile updated successfully!']);
	}
}
