<?php namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{

	/**
	 * @return Model
	 */
    public function get(){
    	
    	return Country::with('states')->get();
    }
}
