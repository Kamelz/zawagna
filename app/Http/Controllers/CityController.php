<?php namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
	 * @return Model
	 */
    public function get($state){
    	
    	return City::where('state_id',$state)->get();
    }
}
