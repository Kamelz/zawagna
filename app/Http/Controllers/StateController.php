<?php namespace App\Http\Controllers;

use App\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
	/**
	 * @return Model
	 */
    public function get($country){
    	
    	return State::where('country_id',$country)->with('cities')->get();
    }
}
