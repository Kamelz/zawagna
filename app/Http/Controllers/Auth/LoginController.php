<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Image;
use Storage;
use App\User;
use Socialite;
use Carbon\Carbon;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')
        ->scopes([
            'email',
            'user_age_range',
            'user_birthday',
            'user_gender',
            'user_photos',
            'user_link'
        ])
        ->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            
            $facebookUser = Socialite::driver('facebook')
            ->fields([
                'first_name', 'last_name','picture', 'email', 'gender', 'birthday'
            ])->user();

        } catch (Exception $e) {

            return redirect('/login')->with(['error'=>'An error occurred during logging in.']);    
        }
                 
        $user = User::where('facebook_id',$facebookUser->getId())->first();
        
        if(!$user){
            $imagePath =  $this->storeUserImage($facebookUser);

            $birthday = (new Carbon($facebookUser->user['birthday']))->format('Y-m-d');

            $age = $this->getAge($facebookUser->user['birthday']);

            $user = User::create([
                'name' => $facebookUser->user['first_name'].' '.$facebookUser->user['last_name'],
                'email' => $facebookUser->getEmail(),
                'facebook_id' => $facebookUser->getId(),
                'gender' => $facebookUser->user['gender'],
                'birthday' => $birthday,
                'profile_picture' => $imagePath??null,
                'age' => $age,
            ]);
        }

        Auth::loginUsingId($user->id);
        return redirect('/home');
    }
    /**
     * 
     * @param   $facebookUser    
     * @return  $photo              
     */
    public function storeUserImage($facebookUser){
        if($facebookUser->user['picture']['data']['is_silhouette']){return;}

        $photo = 'pp_'.$facebookUser->getId().'.jpg';
        $path = storage_path('app\photos'.$photo);
        Image::make($facebookUser->getAvatar())
        ->save($path);

        return $photo;
    }

    /**
     * @param   $birthday
     * @return $age          
     */
    public function getAge($birthday){
       $birthDate = explode("/", $birthday);
        //get age from date or birthdate
        return (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));          
    }
}
