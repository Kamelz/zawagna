<?php namespace App\Traits;

use App\User;
use Illuminate\Support\Facades\Hash;

trait UserRegistrationTrait{

	public function signUp($data){

		$user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'country_id' => $data['country_id'],
            'state_id' => $data['state_id'],
            'city_id' => $data['city_id'],
            'gender' => $data['gender'],
            'birthday' => $data['birthday'],
            'age' => $this->getAge($data['birthday']),
            'password' => Hash::make($data['password']),
        ]);
		
		if(null === request('profile_picture')) return $user;
		
		if(request()->file('profile_picture')->isValid()){
			$path = 'pp_'.$user->id.'.'.request()->profile_picture->extension();
			request()->profile_picture->storeAs('photos',$path);
		}

		$user->profile_picture = $path ?? null;

		$user->save(); 

        return $user;
	}

	public function facebookSignUp(){
		
	}

	// ---------------- Helper metods -----------------------------//
	
	/**
	 * @param   $birthday 
	 * @return   string         
	 */
	public function getAge($birthday){
	   if(!$birthday) return null;
       $birthDate = explode("-", $birthday);

       return (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
        ? ((date("Y") - $birthDate[0]) - 1)
        : (date("Y") - $birthDate[0]));         
    } 
}