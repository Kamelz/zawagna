<?php

namespace App;
use App\State;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    /**
     * @return hasMany
     */
    public function states(){

    	return $this->hasMany(State::class);
    }
}
